export const environment = {
  production: true,
  apiUrl: 'https://algamoney-backend-api.herokuapp.com',

  // domínios que permito enviar token
  tokenWhitelistedDomains: [new RegExp('algamoney-backend-api.herokuapp.com')],
  // tokenWhitelistedDomains: [ new RegExp('localhost:8080') ],
  // domínios que não permito enviar token
  tokenBlacklistedRoutes: [new RegExp('/oauth/token')],
};
