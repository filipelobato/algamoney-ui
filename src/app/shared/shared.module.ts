import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrimOnBlurDirective } from './trim-on-blur.directive';
import { MessageComponent } from './message/message.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MessageComponent, TrimOnBlurDirective],
  exports: [MessageComponent, TrimOnBlurDirective]
})
export class SharedModule { }
