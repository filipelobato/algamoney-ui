import { FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message',
  template: `
    <div *ngIf="temErro()" class="ui-message ui-widget ui-corner-all ui-message-error ng-star-inserted espaco-acima">
      <span class="ui-message-icon pi pi-times"></span>
      <span class="ui-message-text ng-star-inserted">{{ text }}</span>
    </div>
  `,
  styles: [`
    .ui-messages-error {
      margin: 0;
      margin-top: 4px;
    }
    .espaco-acima {
      background-color: #f8b7bd;
      border: 0 none;
      color: #212121;
      margin-top: 0.15em;
  }
  `]
})
export class MessageComponent {

  @Input() error: string;
  @Input() control: FormControl;
  @Input() text: string;

  temErro(): boolean {
    return this.control.hasError(this.error) && this.control.dirty;
  }

}
