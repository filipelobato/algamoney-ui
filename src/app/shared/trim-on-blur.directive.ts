import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: 'input[trimOnBlur]'
})
export class TrimOnBlurDirective {

  private dispatchEvent (el, eventType) {
    const event = document.createEvent('Event');
    event.initEvent(eventType, false, false);
    el.dispatchEvent(event);
  }

  @HostListener('blur', [ '$event.target', '$event.target.value' ])
  onBlur (el: any, value: string): void {
      el.value = value.replace( /  +/g, ' ' ).trim();

      this.dispatchEvent(el, 'input');
  }
}
