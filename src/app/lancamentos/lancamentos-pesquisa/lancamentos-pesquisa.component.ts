import { AuthService } from './../../seguranca/auth.service';
import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { MessageService, ConfirmationService, LazyLoadEvent } from 'primeng/api';

import { LancamentoService, LancamentoFiltro } from '../lancamento.service';
import { ErrorHandlerService } from './../../core/error-handler.service';


@Component({
  selector: 'app-lancamentos-pesquisa',
  templateUrl: './lancamentos-pesquisa.component.html',
  styleUrls: ['./lancamentos-pesquisa.component.css']
})
export class LancamentosPesquisaComponent implements OnInit, OnDestroy {
  totalRegistros = 0;
  filtro = new LancamentoFiltro();
  filtroEnviado = new LancamentoFiltro();
  lancamentos = [];
  @ViewChild('tabela', { static: true }) grid;
  firstPaginaAtual: number;
  polling: any;

  constructor(
    private lancamentoService: LancamentoService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private title: Title,
    public auth: AuthService
  ) { }

  ngOnInit() {
    // this.pesquisar();
    this.title.setTitle('Pesquisa de lançamentos');
    //this.iniciarRefreshPage();
  }

  iniciarRefreshPage() {
    this.polling = setInterval(() => {
      const pagina = this.grid.first / this.grid.rows;
      this.atualizar(pagina);
    }, 5000);
  }

  finalizarRefreshPage() {
    clearInterval(this.polling);
  }

  limparForm() {
    this.filtro = new LancamentoFiltro();
  }

  ngOnDestroy() {
    //this.finalizarRefreshPage();
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;

    this.lancamentoService.pesquisar(this.filtro)
      .then((resultado) => {
        this.lancamentos = resultado.lancamentos;
        this.totalRegistros = resultado.total;
        this.filtroEnviado.pagina = this.filtro.pagina;
        this.filtroEnviado.descricao = this.filtro.descricao;
        this.filtroEnviado.dataVencimentoInicio = this.filtro.dataVencimentoInicio;
        this.filtroEnviado.dataVencimentoFim = this.filtro.dataVencimentoFim;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina(event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  confirmarExclusao(lancamento: any) {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.excluir(lancamento);
      }
    });
  }

  excluir(lancamento: any) {
    const pagina = this.grid.first / this.grid.rows;
    this.lancamentoService.excluir(lancamento.codigo)
      .then(() => {
        if (this.quantidadeRegistroDaPagina() > 1) {
          this.pesquisar(pagina);
        } else if (this.quantidadeRegistroDaPagina() === 1 && pagina > 0) {
          this.grid.first = this.grid.first - this.grid.rows;
          this.pesquisar(pagina - 1);
        } else {
          this.pesquisar(pagina);
        };

        this.messageService.add({ severity: 'success', detail: 'Lançamento excluído com sucesso!' });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  quantidadeRegistroDaPagina(): number {
    return this.grid.value.length;
  }

  atualizar(pagina = 0) {
    this.filtroEnviado.pagina = pagina;

    this.lancamentoService.pesquisar(this.filtroEnviado)
      .then((resultado) => {
        this.lancamentos = resultado.lancamentos;
        this.totalRegistros = resultado.total;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
