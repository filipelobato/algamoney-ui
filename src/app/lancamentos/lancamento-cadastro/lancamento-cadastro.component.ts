import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageService } from 'primeng/api';

import { LancamentoFiltro, LancamentoService } from './../lancamento.service';
import { PessoasService, PessoaFiltro } from './../../pessoas/pessoas.service';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { CategoriaService } from './../../categorias/categoria.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-lancamento-cadastro',
  templateUrl: './lancamento-cadastro.component.html',
  styleUrls: ['./lancamento-cadastro.component.css']
})
export class LancamentoCadastroComponent implements OnInit {

  tipos = [
    { label: 'Receita', value: 'RECEITA' },
    { label: 'Despesa', value: 'DESPESA' },
  ];

  categorias = [];

  pessoas = [];

  // lancamento = new Lancamento();
  formulario: FormGroup;
  uploadEmAndamento = false;

  constructor(
    private categoriaService: CategoriaService,
    private pessoasService: PessoasService,
    private lancamentoService: LancamentoService,
    private errorHandlerService: ErrorHandlerService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.configurarFormulario();
    const codigoLancamento = this.route.snapshot.params['codigo'];

    if (codigoLancamento) {
      this.carregarLancamento(codigoLancamento);
    } else {
      this.title.setTitle('Novo lançamento');
    }

    this.carregarCategorias();
    this.carregarPessoas();
  }

  validarArquivo(event: any, fileUpload: any) {
    const formatosSuportados = this.configurarFormatosSuportados(fileUpload);
    const formatoArquivoEnviado = event.files[0].type;

    if (!this.validarFormatoArquivoEnviado(formatosSuportados, formatoArquivoEnviado)) {
      this.messageService.add({
        severity: 'error', summary: 'Erro ao tentar enviar anexo.',
        detail: `Formato de arquivo não aceito. Formatos suportados: imagens (.jpg, .jpeg, .png)`
      });
    }

    if (event.files[0].size > fileUpload.maxFileSize) {
      this.messageService.add({
        severity: 'error', summary: 'Erro ao tentar enviar anexo',
        detail: `Tamanho máximo do arquivo excedido. Limite:  ${this.converterBytesParaMB(fileUpload.maxFileSize)}`
      });
    }

  }

  configurarFormatosSuportados(fileUpload: any): string[] {
    const formatadosSuportados = fileUpload.accept.split(',');

    const formatosSuportadosFormatados = formatadosSuportados.map(tipo => {
      return tipo.includes('/*') ? tipo.replace('/*', '') : tipo;
    })

    return formatosSuportadosFormatados;
  }

  validarFormatoArquivoEnviado(formatosSuportados: string[], formatoArquivoEnviado: string): boolean {
    let arquivoSuportado = false;
    formatosSuportados.forEach(formatoSuportado => {
      if (formatoArquivoEnviado.includes(formatoSuportado)) {
        arquivoSuportado = true;
      }
    });
    return arquivoSuportado;
  }

  converterBytesParaMB(tamanhoEmBytes: number): string {
    return tamanhoEmBytes / 1048576 + 'MB';
  }

  aoTerminarUploadAnexo(event) {
    const anexo = event.originalEvent.body;

    this.formulario.patchValue({
      anexo: anexo.nome,
      urlAnexo: anexo.url
    })
    this.messageService.add({
      severity: 'info', summary: 'A  nexado com sucesso!',
      detail: 'É necessário salvar para completar alteração.'
    });

    this.uploadEmAndamento = false;
  }

  erroUpload(event) {
    this.uploadEmAndamento = false;
    this.messageService.add({ severity: 'error', summary: 'Erro ao tentar enviar anexo', detail: '' });
  }

  removerAnexo() {
    this.formulario.patchValue({
      anexo: null,
      urlAnexo: null
    })
  }

  get nomeAnexo() {
    const nome = this.formulario.get('anexo').value;

    if (nome) {
      return nome.substring(nome.indexOf('_') + 1, nome.length);
    }
    return '';
  }

  get urlAnexo() {
    return 'https:' + this.formulario.get('urlAnexo').value;
  }

  get urlUploadAnexo() {
    return this.lancamentoService.urlUploadAnexo();
  }

  configurarFormulario() {
    this.formulario = this.formBuilder.group({
      codigo: [],
      tipo: ['RECEITA', Validators.required],
      dataVencimento: [null, Validators.required],
      dataPagamento: [],
      descricao: [null, [this.validarObrigatoriedade, this.validarTamanhoMinimo(5)]],
      valor: [null, Validators.required],
      pessoa: this.formBuilder.group({
        codigo: [null, Validators.required],
        nome: []
      }),
      categoria: this.formBuilder.group({
        codigo: [null, Validators.required],
        nome: []
      }),
      observacao: [],
      anexo: [],
      urlAnexo: []
    });

  }

  validarObrigatoriedade(input: FormControl) {
    // const dtVencimento = input.root.get('dataVencimento').value;
    return (input.value ? null : { obrigatoriedade: true });
  }

  validarTamanhoMinimo(valor: number) {
    return (input: FormControl) => {
      return (!input.value || input.value.length >= valor) ? null : { tamanhoMinimo: { tamanho: valor } };
    };
  }

  carregarCategorias() {
    this.categoriaService.listarTodas()
      .then((categorias) => {
        this.categorias = categorias.map(c => ({ label: c.nome, value: c.codigo }));
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  salvar() {
    if (this.editando) {
      this.atualizarLancamento();
    } else {
      this.adicionarLancamento();
    }
  }

  adicionarLancamento() {
    this.lancamentoService.adicionar(this.formulario.value)
      .then(lancamentoAdicionado => {
        this.messageService.add({ severity: 'success', detail: 'Lançamento adicionado com sucesso!' });
        // form.reset(form.value);
        // this.lancamento = new Lancamento();

        this.router.navigate(['/lancamentos', lancamentoAdicionado.codigo])
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  atualizarLancamento() {
    this.lancamentoService.atualizar(this.formulario.value)
      .then(lancamento => {
        this.formulario.patchValue(lancamento);

        this.messageService.add({ severity: 'success', detail: 'Lançamento alterado com sucesso!' });
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  carregarPessoas() {
    const filtro = new PessoaFiltro;
    this.pessoasService.listarTodas()
      .then((pessoas) => {
        this.pessoas = pessoas.map(p => ({ label: p.nome, value: p.codigo }));
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  carregarLancamento(codigo: number) {
    this.lancamentoService.buscarPorCodigo(codigo)
      .then(lancamento => {
        this.formulario.patchValue(lancamento);
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  get editando() {
    return Boolean(this.formulario.get('codigo').value);
  }

  novo() {
    this.formulario.reset(this.formulario.value);

    // this.Lancamento = new Lancamento();
    this.router.navigate(['lancamentos/novo']);
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de lançamento: ${this.formulario.get('descricao').value}`);
  }
}
