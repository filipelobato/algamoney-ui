import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild } from '@angular/core';

import { LazyLoadEvent, ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import { AuthService } from './../../seguranca/auth.service';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { PessoaFiltro, PessoasService } from './../pessoas.service';

@Component({
  selector: 'app-pessoas-pesquisa',
  templateUrl: './pessoas-pesquisa.component.html',
  styleUrls: ['./pessoas-pesquisa.component.css']
})
export class PessoasPesquisaComponent implements OnInit {

  totalRegistros = 0;
  filtro = new PessoaFiltro;
  pessoas = [];
  @ViewChild('tabela', { static: true }) grid;

  // @Input() pessoas = [];
  constructor(
    private pessoasService: PessoasService,
    private confirmarionService: ConfirmationService,
    private messageService: MessageService,
    private errorHandler: ErrorHandlerService,
    private auth: AuthService,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('Pesquisa de pessoas')
  }

  pesquisar(pagina = 0) {
    this.filtro.pagina = pagina;

    this.pessoasService.pesquisar(this.filtro)
      .then(resultado => {
        this.totalRegistros = resultado.total;
        this.pessoas = resultado.pessoas;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina(evento: LazyLoadEvent) {
    const pagina = evento.first / evento.rows;
    this.pesquisar(pagina);
  }

  excluir(pessoa: any) {
    const pagina = this.grid.first / this.grid.rows;

    this.pessoasService.excluir(pessoa.codigo)
      .then(() => {
        if (this.ultimoRegistroExcluidoDaPagina()) {
          this.retrocederUmaPagina();
        } else {
          this.atualizarPagina(pagina);
        }
        this.messageService.add({ severity: 'success', detail: 'Pessoa excluída com sucesso!' });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  ultimoRegistroExcluidoDaPagina() {
    const pagina = this.grid.first / this.grid.rows;

    if (this.grid.value.length === 1 && pagina > 0) {
      return true;
    }

    return false;
  }

  retrocederUmaPagina() {
    const pagina = this.grid.first / this.grid.rows;

    this.grid.first = this.grid.first - this.grid.rows;
    const paginaAnterior = pagina - 1;
    this.pesquisar(paginaAnterior);
  }

  atualizarPagina(pagina) {
    this.pesquisar(pagina);
  }

  confirmarExclusao(pessoa: any): void {
    this.confirmarionService.confirm({
      message: 'Tem certeza que deseja excluir??',
      accept: () => {
        this.excluir(pessoa);
      }
    });
  }

  alterarStatus(pessoa: any): void {
    const novoStatus = !pessoa.ativo;

    this.pessoasService.alterarStatus(pessoa.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'ativada' : 'desativada';

        pessoa.ativo = novoStatus;
        this.messageService.add({ severity: 'success', detail: `Pessoa ${acao} com sucesso!` });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  temPermissao(permissao: string): boolean {
    return this.auth.temPermissao(permissao);
  }

  limparForm() {
    this.filtro = new PessoaFiltro();
  }

}
