import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

import { MessageService } from 'primeng/api';

import { Pessoa, Contato, Estado, Cidade } from './../../core/model';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { PessoasService } from './../pessoas.service';

@Component({
  selector: 'app-pessoa-cadastro',
  templateUrl: './pessoa-cadastro.component.html',
  styleUrls: ['./pessoa-cadastro.component.css']
})
export class PessoaCadastroComponent implements OnInit {

  pessoa = new Pessoa();
  estados: any[];
  cidades: any[];
  estadoSelecionado: number;
  cidadeSelecionada: number;
  @ViewChild('p', { static: true }) form: NgForm;

  constructor(
    private pessoasService: PessoasService,
    private errorHandlerService: ErrorHandlerService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private title: Title
  ) { }

  ngOnInit() {
    const codigoPessoa = this.route.snapshot.params['codigo'];

    this.carregarEstados();

    if (codigoPessoa) {
      this.carregarPessoa(codigoPessoa);
    } else {
      this.title.setTitle('Nova pessoa');
    }
  }

  salvar(form: FormControl) {
    if (this.cidadeSelecionada) {
      this.pessoa.endereco.cidade = new Cidade(this.cidadeSelecionada);
    }

    if (this.editando) {
      this.atualizarPessoa(form);
    } else {
      this.adicionarPessoa(form);
    }
  }

  adicionarPessoa(form: FormControl) {
    this.pessoasService.adicionar(this.pessoa)
      .then((pessoaAdicionada) => {
        this.messageService.add({severity: 'success', detail: 'Pessoa adicionada com sucesso!'});

        // form.reset(form.value);
        // this.pessoa = new Pessoa();
        this.form.reset(this.form.value);
        this.router.navigate(['/pessoas', pessoaAdicionada.codigo])
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  atualizarPessoa(form: FormControl) {
    this.pessoasService.atualizar(this.pessoa)
      .then(pessoa => {
        this.pessoa = pessoa;

        this.messageService.add({severity: 'success', detail: 'Pessoa alterada com sucesso!'});
        this.form.reset(this.form.value);
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  carregarPessoa(codigo: number) {
    this.pessoasService.buscarPorCodigo(codigo)
      .then((pessoa) => {
        this.pessoa = pessoa;

        this.estadoSelecionado = this.pessoa.endereco.cidade ?
          this.pessoa.endereco.cidade.estado.codigo : null;

          if (this.estadoSelecionado) {
            this.carregarCidades();
          }

          if (pessoa.endereco.cidade) {
            this.cidadeSelecionada = this.pessoa.endereco.cidade.codigo;
          }

        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de pessoa: ${this.pessoa.nome}`);
  }

  novo(form: FormControl) {
    form.reset(form.value);
    this.pessoa = new Pessoa();

    this.router.navigate(['pessoas/novo']);
  }

  get editando() {
    return Boolean(this.pessoa.codigo);
  }

  carregarEstados() {
    this.pessoasService.listarEstados()
      .then(estados => {
        this.estados = estados.map(uf => ({ label: uf.nome, value: uf.codigo}))
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  carregarCidades() {
    this.pessoasService.pesquisarCidades(this.estadoSelecionado)
      .then(cidades => {
        this.cidades = cidades.map(cidade => ({ label: cidade.nome, value: cidade.codigo}))
        if (this.cidades.length === 0) {
          this.cidadeSelecionada = null;
        }
      })
      .catch(erro => this.errorHandlerService.handle(erro));
  }

  get salvo() {
    return this.pessoa.codigo && !(this.form.dirty);
  }

}
