import { FormControl } from '@angular/forms';
import { Contato } from './../../core/model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pessoa-cadastro-contato',
  templateUrl: './pessoa-cadastro-contato.component.html',
  styleUrls: ['./pessoa-cadastro-contato.component.css']
})
export class PessoaCadastroContatoComponent implements OnInit {

  @Input() contatos: Array<Contato>;
  @Input() frmPessoa;
  contato: Contato;
  exibindoFormularioContato = false;
  contatoIndex: number;

  constructor() { }

  ngOnInit() {
  }

  prepararNovoContato() {
    this.exibindoFormularioContato = true;
    this.contato = new Contato();
    this.contatoIndex = this.contatos.length;
  }

  prepararEdicaoContato(contato: Contato, index: number) {
    this.contato = this.clonarContato(contato);
    this.exibindoFormularioContato = true;
    this.contatoIndex = index;
  }

  confirmarContato(frmContato: FormControl) {
    // Marca como form Alterado
    this.frmPessoa.form.markAsDirty();
    this.contatos[this.contatoIndex] = this.clonarContato(this.contato);

    this.exibindoFormularioContato = false;

    frmContato.reset(frmContato.value);
  }

  removerContato(index: number) {
    this.contatos.splice(index, 1);
    this.frmPessoa.form.markAsDirty();
  }


  clonarContato(contato: Contato) {
    return new Contato(contato.codigo,
      contato.nome, contato.email, contato.telefone)
  }

  get editando() {
    return Boolean(this.contato.codigo);
  }

  formatarTelefone(numero: string): string {
    if (numero.length === 11) {
      return `(${numero.slice(0, 2)}) ${numero.slice(2, 7)}-${numero.slice(7, 11)}`
    }
    return `(${numero.slice(0, 2)}) ${numero.slice(2, 6)}-${numero.slice(6, 10)}`
  }

}
