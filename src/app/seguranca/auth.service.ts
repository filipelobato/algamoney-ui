import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { JwtHelperService } from '@auth0/angular-jwt';

import { environment } from './../../environments/environment';
import { tokenGetter } from './seguranca.module';

@Injectable()
export class AuthService {

  oauthTokenURL: string;
  jwtPayload: any;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private messageService: MessageService,
    private router: Router,
  ) {
    this.oauthTokenURL = `${environment.apiUrl}/oauth/token`;
    this.carregarToken();
  }

  login(usuario: string, senha: string): Promise<void> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/x-www-form-urlencoded')
      .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==');

    const body = `username=${usuario}&password=${senha}&grant_type=password`;

    return this.http.post<any>(this.oauthTokenURL, body, { headers, withCredentials: true })
      .toPromise()
      .then(response => {
        this.armazenarToken(response.access_token);
      })
      .catch(response => {
        if (response.status === 400) {
          if (response.error.error === 'invalid_grant') {
            return Promise.reject('Usuario  ou senha inválidos');
          }
        }

        return Promise.reject(response);
      });
  }

  /*  logout(usuario: string, senha: string): Promise<void> {
      const headers = new HttpHeaders()
        .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==')
        .append('Content-Type', 'application/x-www-form-urlencoded');

      const body = `username=${usuario}&password=${senha}&grant_type=password`;

      return this.http.post<any>(this.oauthTokenURL, body, { headers })
        .toPromise()
        .then(response => {
          this.armazenarToken(response.access_token);
        })
        .catch(response => {
          if (response.status === 400) {
            if (response.error === 'invalid_grant') {
              return Promise.reject('Usuario  ou senha inválidos');
            }
          }
        });
    }
  */
  obterNovoAccessToken(): Promise<void> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==')
      .append('Content-Type', 'application/x-www-form-urlencoded');

    const body = `grant_type=refresh_token`;

    return this.http.post<any>(this.oauthTokenURL, body, { headers, withCredentials: true })
      .toPromise()
      .then(response => {
        this.armazenarToken(response.access_token)

        console.log('Novo access token criado!');

        return Promise.resolve(null);
      })
      .catch(response => {
        console.log('Erro ao renovar Token.', response);

        return Promise.resolve(null);
      });
  }

  limparAccessToken() {
    localStorage.removeItem('token');
    this.jwtPayload = null;
  }

  private armazenarToken(token: string) {
    this.jwtPayload = this.jwtHelper.decodeToken(token);
    localStorage.setItem('token', token);
  }

  private carregarToken() {
    const token = localStorage.getItem('token');

    if (token) {
      this.armazenarToken(token);
    }
  }

  isAccessTokenInvalido() {
    const token = localStorage.getItem('token');
    return !token || this.jwtHelper.isTokenExpired(token);
  }

  isTokenExpired() {
    const token = localStorage.getItem('token');
    return this.jwtHelper.isTokenExpired(token);
  }

  temPermissao(permissao: string) {
    return this.jwtPayload && this.jwtPayload.authorities.includes(permissao);
  }

  temAlgumaPermissao(roles) {
    for (const role of roles) {
      if (this.temPermissao(role)) {
        return true;
      }
    }
    return false;
  }

}
