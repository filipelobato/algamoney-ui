import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { RelatorioLancamentosComponent } from './relatorio-lancamentos/relatorio-lancamentos.component';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  imports: [
    CommonModule,
    RelatoriosRoutingModule,
    FormsModule,
    CalendarModule,
    ProgressSpinnerModule,

    SharedModule
  ],
  declarations: [RelatorioLancamentosComponent]
})
export class RelatoriosModule { }
