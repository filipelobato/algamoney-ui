import { ErrorHandlerService } from './../../core/error-handler.service';
import { RelatoriosService } from './../relatorios.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relatorio-lancamentos',
  templateUrl: './relatorio-lancamentos.component.html',
  styleUrls: ['./relatorio-lancamentos.component.css']
})
export class RelatorioLancamentosComponent implements OnInit {

  periodoInicio: Date;
  periodoFim: Date;
  relatorioEmAndamento = false;

  constructor(
    private relatorioService: RelatoriosService,
    private errorHandler: ErrorHandlerService) { }

  ngOnInit() {
  }

  gerar() {
    this.relatorioEmAndamento = true;
    this.relatorioService.relatorioLancamentosPorPessoa(this.periodoInicio, this.periodoFim)
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);

        window.open(url);
        this.relatorioEmAndamento = false;
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
        this.relatorioEmAndamento = false;
      });

  }
}
